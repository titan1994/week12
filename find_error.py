"""
Скрипт с тестовыми данными пандаса.
Формирует файлы json в различных форматах по данным из пандаса.
Затем проверяет их содержимое
"""

import pandas as pd
import numpy as np
from io import BytesIO


def pandas_data_to_bytes_data_json(dfs, indent=0):
    """
    Преобразовать панду в байтовый поток джсон
    """
    try:
        fo = BytesIO()
        dfs.to_json(fo, indent=indent, force_ascii=False)
        fo.seek(0)

        byte_data = fo.read()
        fo.close()
    except Exception as exp:  # noqa: F841
        return None

    return byte_data


def pandas_data_to_bytes_object_json(dfs, indent=0):
    """
    Преобразовать панду в буфер байт джсон
    """
    try:
        fo = BytesIO()
        dfs.to_json(fo, indent=indent, force_ascii=False)
        fo.seek(0)
    except Exception as exp:  # noqa: F841
        return None

    return fo


def pandas_data_to_file_json(dfs, file_path, indent=0):
    """
    Преобразовать панду в файл джсон
    """

    try:
        dfs.to_json(file_path, indent=indent, force_ascii=False)
    except Exception as exp:  # noqa: F841
        return None

    return file_path


def pandas_data_to_string_json(dfs, indent=0):
    """
    Преобразовать панду в джсон строку
    """
    try:
        res = dfs.to_json(None, indent=indent, force_ascii=False)
    except Exception as exp:  # noqa: F841
        res = None

    return res


simple_series = pd.Series(
    [17.04, 143.5, 9.5, 9.5, 45.5], index=[0, 1, 2, 3, 4]
)

simple_df = pd.DataFrame(
    {
        'country': ['Kazakhstan', 'Russia', 'Russia', 'Belarus', 'Ukraine'],
        'population': simple_series,
        'square': [2724902, 17125191, 207600, 207600, 603628],
    }
)

simple_index_df = pd.DataFrame(
    np.arange(16).reshape(4, 4),
    index=['white', 'black', 'red', 'blue'],
    columns=['up', 'down', 'right', 'left'],
)

multi_index_df = pd.DataFrame(
    np.random.randn(16).reshape(4, 4),
    index=[
        ['белый', 'белый', 'красный', 'красный'],
        ['верх', 'низ', 'верх', 'низ'],
    ],
    columns=[
        # ['doom', 'doom', 'doom', 'doom'],
        ['pen', 'pen', 'paper', 'paper'],
        [1, 2, 2, 1],
    ],
)


def print_all_example_data():
    print('simple_series')
    print(simple_series)
    print('-' * 80)

    print('simple_df')
    print(simple_df)
    print('-' * 80)

    print('simple_index_df')
    print(simple_index_df)
    print('-' * 80)

    print('multi_index_df')
    print(multi_index_df)
    print('-' * 80)


def print_all_example_to_dict_data():
    print('simple_series')
    print(simple_series.to_dict())
    print('-' * 80)

    print('simple_df')
    print(simple_df.to_dict())
    print('-' * 80)

    print('simple_index_df')
    print(simple_index_df.to_dict())
    print('-' * 80)

    print('multi_index_df')
    print(multi_index_df.to_dict())
    print('-' * 80)


def check_out_buffers():
    """
    Проверка трансформации в поток
    """
    print(pandas_data_to_string_json(simple_series, indent=4))
    print(pandas_data_to_string_json(simple_df, indent=4))
    print(pandas_data_to_string_json(simple_index_df, indent=4))
    print(pandas_data_to_string_json(multi_index_df, indent=4))

    print(pandas_data_to_bytes_data_json(simple_series, indent=4))
    print(pandas_data_to_bytes_data_json(simple_df, indent=4))
    print(pandas_data_to_bytes_data_json(simple_index_df, indent=4))
    print(pandas_data_to_bytes_data_json(multi_index_df, indent=4))


def check_out_files():
    """
    Проверка трансформации в файл
    """
    gen_name = __file__
    gen_name = gen_name[:-3]

    pandas_data_to_file_json(
        file_path=f'{gen_name}_simple_series.json', dfs=simple_series, indent=4
    )

    pandas_data_to_file_json(
        file_path=f'{gen_name}_simple_df.json', dfs=simple_df, indent=4
    )

    pandas_data_to_file_json(
        file_path=f'{gen_name}_simple_index_df.json',
        dfs=simple_index_df,
        indent=4,
    )

    pandas_data_to_file_json(
        file_path=f'{gen_name}_multi_index_df.json',
        dfs=multi_index_df,
        indent=4,
    )


def check_in_files():
    gen_name = __file__
    gen_name = gen_name[:-3]

    list_files = [
        f'{gen_name}_simple_df.json',
        f'{gen_name}_simple_index_df.json',
    ]

    for file_name in list_files:
        res = pd.read_json(file_name)
        print(file_name)
        print(res)
        print('-' * 77)


def check_in_files2():
    gen_name = __file__
    gen_name = gen_name[:-3]

    series_name = f'{gen_name}_simple_series.json'
    multi_ind_name = f'{gen_name}_multi_index_df.json'

    res = pd.read_json(series_name, typ='series')
    print(series_name)
    print(res)
    print('-' * 77)

    res = pd.read_json(multi_ind_name)
    print(multi_ind_name)
    print(res)
    print('-' * 77)


if __name__ == '__main__':
    print_all_example_data()
    print('*' * 99)

    print_all_example_to_dict_data()
    print('*' * 99)

    check_out_buffers()
    print('*' * 99)

    check_out_files()
    print('*' * 99)

    check_in_files()
    print('*' * 99)
    check_in_files2()
